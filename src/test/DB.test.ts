let should = require("should");
let monk = require("monk");

let mongoURI = "localhost/testing";

describe("Adding data", function () {
    let db, testClients: any;

    beforeEach(function (done) {
        db = monk(mongoURI);
        testClients = db.get("testClients");
        done();
    });

    after(function (done) {
        monk(mongoURI)
            .get("testClients")
            .drop(function (err: any) {
                if (err) return done(err);
            });
        done();
    });

    it("Should insert data", function (done) {
        testClients.insert({client: "ios", version: "267"}, function (err: any, doc: any) {
            if (err) return done(err);
            should.exists(doc);
            done();
        });
    });
});

describe("Updating data", function () {
    let db, testClients: any;

    beforeEach(function (done) {
        db = monk(mongoURI);
        testClients = db.get("testClients");
        done();
    });

    after(function (done) {
        monk(mongoURI)
            .get("testClients")
            .drop(function (err: any) {
                if (err) return done(err);
            });
        done();
    });

    it("schould insert, update and then get updated value", function (done) {

        testClients.insert({client: "ios", version: "267", border: "12"}, function (err: any, doc: any) {
            if (err) return done(err);
            should.exists(doc);

            testClients.update(
                {client: "ios", version: "267"},
                {$set: {border: "24"}},
                function (err: any) {

                    testClients.findOne({client: "ios", version: "267"}, function (err: any, expectedObject: any) {
                        if (err) return done(err);
                        should.exists(expectedObject);
                        expectedObject.border.should.equal("24");
                    });

                    done();
                });
        });
    });
});