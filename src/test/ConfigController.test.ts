import * as TypeMoq from "typemoq";
import { app } from "../app";
import { agent, SuperTest, Test } from "supertest";
import { DbDriver } from "../DbDriver";
import { Container } from "typescript-ioc";
import { Client } from "../model/Client";
import * as assert from "assert";

const server = app.listen();
let request: SuperTest<Test>;

const mockDriver = TypeMoq.Mock.ofType<DbDriver>();

describe("Test Config Controller", () => {
    before(() => {
        request = agent(server);
        Container.bind(DbDriver).provider({get: () => mockDriver.object});
    });
    after(() => {
        server.close();
    });

    beforeEach(() => {
        mockDriver.reset();
    });

    describe("make POST request to /v1/client/", () => {
        it("should response with 201 result", () => {
            const mockConfig = new Client();
            mockConfig.client = "dummyClient";
            mockConfig.version = "dummyVersion";
            mockConfig.key = "dummyKey";
            mockConfig.value = "dummyValue";

            mockDriver.setup(
                (it: any) => it.store(mockConfig))
                .returns(() => Promise.resolve(mockConfig))
                .verifiable(TypeMoq.Times.exactly(1));

            return request
                .post("/v1/config")
                .expect(201)
                .send(mockConfig)
                .then((response) => {
                    mockDriver.verifyAll();
                    assert.deepEqual(response.body, {});
                });
        });

        it("should response with 400 result", () => {
            const mockConfig = new Client();
            mockConfig.client = "dummyClient";
            mockConfig.version = "dummyVersion";
            mockConfig.key = "dummyKey";
            mockConfig.value = "";

            mockDriver.setup(
                (it: any) => it.store(mockConfig))
                .throws(new Error())
                .verifiable(TypeMoq.Times.exactly(2));

            return request
                .post("/v1/config")
                .expect(400)
                .send(mockConfig)
                .catch((response) => {
                    mockDriver.verifyAll();
                    assert.deepEqual(response.body, {});
                });
        });
    });

    describe("make GET request to /v1/client/", () => {
        it("(existing resource)should response with 200 code and return data.", () => {
            const client = "dummyExistingClient";
            const version = "dummyExistingVersion";

            const expectedResponse = {
                "font-size": "11",
                background_color: "#011",
                "border-left": "-3px"
            };

            mockDriver.setup(
                (it: any) => it.find(client, version))
                .returns(() => Promise.resolve(expectedResponse))
                .verifiable(TypeMoq.Times.exactly(1));

            return request
                .get(`/v1/config/${client}/${version}`)
                .expect(200)
                .then((response) => {
                    mockDriver.verifyAll();
                    assert.deepEqual(response.body, expectedResponse);
                });
        });
        it("(missing resource)should response with 204 code.", () => {
            const client = "dummyNonExistingClient";
            const version = "dummyNonExistingVersion";

            mockDriver.setup(
                (it: any) => it.find(client, version))
            // tslint:disable-next-line:no-null-keyword
                .returns(() => Promise.resolve(null))
                .verifiable(TypeMoq.Times.exactly(1));

            return request
                .get(`/v1/config/${client}/${version}`)
                .expect(204)
                .then((response) => {
                    mockDriver.verifyAll();
                    assert.deepEqual(response.body, {});
                });
        });
    });
});