import { IsString, MinLength } from "class-validator";

export class Client {
    @IsString()
    @MinLength(1)
    client: string;

    @IsString()
    @MinLength(1)
    version: string;

    @IsString()
    @MinLength(1)
    key: string;

    @IsString()
    @MinLength(1)
    value: string;
}