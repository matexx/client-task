import { Singleton } from "typescript-ioc";
import * as Debug from "debug";

@Singleton
export class Logger {
    public log = Debug("client:log");
    public error =  Debug("client:error");
    public warn = Debug("client:warn");
}