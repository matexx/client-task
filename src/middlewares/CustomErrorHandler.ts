import { Middleware, ExpressErrorMiddlewareInterface } from "routing-controllers";
import { Logger } from "../Logger";

const logger = new Logger();

@Middleware({ type: "after" })
export class CustomErrorHandler implements ExpressErrorMiddlewareInterface {

    error(error: any, request: any, response: any, next: (err?: any) => any) {
        logger.error(error.message);
        response.sendStatus(error.httpCode || 500);
    }
}