import "reflect-metadata";
import { createExpressServer } from "routing-controllers";
import { DbDriver } from "./DbDriver";
import { Container } from "typescript-ioc";
import { Logger } from "./Logger";

require("dotenv").config();

const logger = new Logger();
const port = process.env.PORT || 3000;
const apiVersion = process.env.API_VERSION || "v1";
const collectionName = process.env.COLLECTION_NAME || "clients";

const swaggerTools = require("swagger-tools");
const YAML = require("yamljs");
const swaggerDocument = YAML.load(__dirname + "/swagger/api-swagger-task.yaml");

logger.log(`App connecting to DB on: ${process.env.MONGODB_URI}`);
const collection = require("monk")(process.env.MONGODB_URI).get(collectionName);
Container.bind(DbDriver).provider({
    get: () => {
        return new DbDriver(collection);
    }
});

export const app = createExpressServer({
    routePrefix: `/${apiVersion}`,
    defaultErrorHandler: false,
    controllers: [__dirname + "/controllers/*.js", __dirname + "/controllers/*.ts"],
    middlewares: [__dirname + "/middlewares/*.js", __dirname + "/middlewares/*.ts"]
});

swaggerTools.initializeMiddleware(swaggerDocument, function (middleware: any) {
    app.use(`/${apiVersion}`, middleware.swaggerUi());
});

if (!module.parent) {
    app.listen(port, (err: any) => {
        if (err) {
            logger.error(`App error: ${err}`);
        }
        logger.log(`App listen on port: ${port}`);
    });
}