import { Body, Get, JsonController, Param, Post, Res } from "routing-controllers";
import { Response } from "express";
import { Client } from "../model/Client";
import { Inject } from "typescript-ioc";
import { DbDriver } from "../DbDriver";
import { Logger } from "../Logger";

const logger = new Logger();

@JsonController("/config")
export class ConfigController {

    @Inject
    private driver: DbDriver;

    @Get("/:client/:version")
    public async getConfigAction(@Param("client") client: string, @Param("version") version: string) {
        logger.log(`Called getConfigAction(${client}, ${version})`);
        try {
            return await this.driver.find(client, version);
        } catch (error) {
            logger.error(`getConfigAction: ${error.message}`);
            throw new Error(error);
        }
    }

    @Post("/")
    public async postConfigAction(@Body({validate: true}) client: Client, @Res() response: Response) {
        logger.log(`Called postConfigAction(${JSON.stringify(client)})`);
        try {
            await this.driver.store(client);
            return response.sendStatus(201);
        } catch (error) {
            logger.error(`postConfigAction: ${error.message}`);
            throw new Error(error);
        }
    }
}