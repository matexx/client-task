import { Client } from "./model/Client";
import { ICollection } from "monk";

export class DbDriver {
    private collection: ICollection = undefined;

    constructor(collectionToSet: ICollection) {
        this.collection = collectionToSet;
    }

    find(client: string, version: string) {
        try {
            return this.collection.findOne({client: client, version: version}, {_id: 0, client: 0, version: 0});
        } catch (error) {
            throw Error(error);
        }
    }

    store(model: Client) {
        try {
            return this.collection.update(
                {client: model.client, version: model.version},
                {$set: {[model.key]: model.value}},
                {upsert: true, multi: false}
            );
        } catch (error) {
            throw new Error(error);
        }
    }
}