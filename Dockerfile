FROM node:8.9.2
MAINTAINER Mateusz Orłowski

WORKDIR /usr/src/app

COPY dist .

CMD ["node","./app.js"]