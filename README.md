## REST API task

The main objective of the task is to create simple API to get and add data according to provided swagger specification.

#### Essential info
* Heroku url: https://damp-castle-72934.herokuapp.com/v1/config/
* Swagger url: https://damp-castle-72934.herokuapp.com/v1/docs/
* Local url: https://localhost:3000/v1/config/

#### Beginning

These instructions will show you how to get a copy of the project up and running on your local machine. See deployment for information, where you can test it online.

#### Using

Exposed endpoint:

* GET: /config/{client}/{version}

Get all changed config fields since the last received change for a specific client and version.

* POST: /config 

Add new or modify existing config fields for a specific client and version.

More descriptive should be swagger documentation endpoint (applied yaml file that was attached to task) 

https://damp-castle-72934.herokuapp.com/v1/docs/

#### Tech

Used technologies and tools
```
* Node.js 
* Npm package manager
* Mongo DB
* Monk
* Typescript
* Tslint
* Express
* Mocha
* Debug
```

I have decided to build my solution based on Node js with Express framework. I've used it before and it works good. As a storage i have used Mongo DB - in my opinion for this kind of data it fits very well. Debug tool I have added to provided simple logging.

#### Installing 

* Required .env file

Just change .env.example => .env filename and ensure, that MONGODB_URI variable pointing into existing and running Mongo Db instance.

###### Run via docker:
1. run **npm run build** - to prepare code for docker
2. next **docker-compose up** 

then service should be avaible at (based on default settings): 
https://localhost:3000/v1/config/

###### Run via npm:
1. run **npm run serve:dev** (assuming, that MONGODB_URI is set properly)

I this case, same as above service should be avaible at (based on default settings): 
https://localhost:3000/v1/config/

#### Deployment

Api is deployed on Heroku cloud:
https://damp-castle-72934.herokuapp.com/v1/config/

#### Testing
Tests location: /test/
Running tests: npm run test

You can call api from swagger https://damp-castle-72934.herokuapp.com/v1/docs/

If You preffer You can use prepared Postman requests collections:

* Heroku instance requests: https://www.getpostman.com/collections/2b77fe835c6920017068

* Local requests: https://www.getpostman.com/collections/d82ed2c3f8a62cf0705b

#### Authors

**Mateusz Orłowski** - (orlowski.mateusz@gmail.com)